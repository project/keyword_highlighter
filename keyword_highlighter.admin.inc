<?php
/**
 * @file
 * Author: Dharmendra Patri
 * This module will allow users to provide keyword and specific
 * colors to be used for highlighting them.
 */

/**
 * Admin form, where admin will be allowed to enter keywords and colors.
 *
 * Builds the form.
 */
function keyword_highlighter_add_keywords_form($form, &$form_state) {
  $form['#attached']['css'][drupal_get_path('module', 'keyword_highlighter') . '/keyword_highlighter.admin.css'] = array();
  $form['description'] = array(
    '#markup' => '<div>' . t('Add the keywords and there relevant color here.') . '</div>',
  );

  // Because we have many fields with the same values, we have to set
  // #tree to be able to access them.
  $form['#tree'] = TRUE;
  $form['names_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add keywords and color'),
    // Set up the wrapper so that AJAX will be able to replace the fieldset.
    '#prefix' => '<div id="names-fieldset-wrapper">',
    '#suffix' => '</div>',
  );

  // Get data from data base and build the number of fields.
  $kh_data_array = variable_get('kh_data_details', '');

  if (!empty($kh_data_array)) {
    $data = $kh_data_array;
  }
  if (!isset($form_state['num_names']) && !empty($kh_data_array)) {
    $form_state['num_names'] = count($data);
  }

  // Build the fieldset with the proper number of names.
  // $form_state['num_names'] to determine the number of textfields to build.
  if (empty($form_state['num_names'])) {
    $form_state['num_names'] = 1;
  }
  for ($i = 0; $i < $form_state['num_names']; $i++) {
    // If data is there - get the data.
    $keyword_value = (isset($data[$i]['keyword']))?$data[$i]['keyword'] : '';
    $color_value   = (isset($data[$i]['color']))?$data[$i]['color'] : '';
    $header = '<div class="two-col tch">
               <div class="col1 ch">' . t('Delete') . '</div>
               <div class="col2 ch">' . t('Keyword') . '</div>
               <div class="col3 ch">' . t('Color') . '</div>
               <div class="clear"></div>
               </div>
               <div class="two-col">';

    $form['names_fieldset'][$i] = array(
      '#prefix' => ($i == 0) ? $header : '<div class="two-col">',
      '#suffix' => '</div>',
    );

    $form['names_fieldset'][$i]['select'] = array(
      '#type' => 'checkbox',
      '#prefix' => '<div class="col1">',
      '#suffix' => '</div>',
    );

    $form['names_fieldset'][$i]['keyword'] = array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 30,
      '#title' => t('Keyword'),
      '#title_display' => 'invisible',
      '#default_value' => $keyword_value,
      '#description' => t('Enter a keyword. Ex- Drupal'),
      '#prefix' => '<div class="col2">',
      '#suffix' => '</div>',
    );

    $form['names_fieldset'][$i]['color'] = array(
      '#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 15,
      '#maxlength' => 7,
      '#title' => t('Color'),
      '#title_display' => 'invisible',
      '#default_value' => $color_value,
      '#description' => t('Enter any hexadecimal color. Ex- #FF00DD'),
      '#prefix' => '<div class="col3">',
      '#suffix' => '</div><div class="clear"></div>',
    );
  }

  $form['names_fieldset']['add_name'] = array(
    '#type' => 'submit',
    '#value' => t('Add one more'),
    '#title' => t('Color'),
    '#submit' => array('keyword_highlighter_add_keywords_form_add_one'),
    '#ajax' => array(
      'callback' => 'keyword_highlighter_add_keywords_form_callback',
      'wrapper' => 'names-fieldset-wrapper',
    ),
  );

  $form['names_fieldset']['remove_name'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Remove Unsaved'), 'admin/config/content/keywords') . '&nbsp;' . t('(Removes all unsaved fields, if any)'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save and delete selected ones'),
  );

  return $form;
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function keyword_highlighter_add_keywords_form_add_one($form, &$form_state) {
  $form_state['num_names']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function keyword_highlighter_add_keywords_form_callback($form, $form_state) {
  return $form['names_fieldset'];
}

/**
 * Validate handler for keyword_highlighter_add_keywords_form().
 */
function keyword_highlighter_add_keywords_form_validate($form, &$form_state) {
  $keyword_arr = $form_state['values']['names_fieldset'];
  // Now check each color to validate the hexadecimal value.
  foreach ($keyword_arr as $key => $kr) {
    if (is_array($kr)) {
      if (!_keyword_highlighter_validate_hexacolor($kr['color']) && $key != 'add_name') {
        form_set_error('color', t('Please provide a valid hexadecimal color. Ex - #00FF00'));
      }
    }
  }
}

/**
 * Final submit handler.
 *
 * Reports what values were finally set.
 */
function keyword_highlighter_add_keywords_form_submit($form, &$form_state) {
  if (count($form_state['values']['names_fieldset']) > 0) {
    // Unset the unwanted values if there.
    if (isset($form_state['values']['names_fieldset']['add_name'])) {
      unset($form_state['values']['names_fieldset']['add_name']);
    }

    // Now check the unchecked ones and remove them.
    for ($i = 0; $i < count($form_state['values']['names_fieldset']); $i++) {
      if ($form_state['values']['names_fieldset'][$i]['select'] == '1') {
        unset($form_state['values']['names_fieldset'][$i]);
      }
    }
    variable_set("kh_data_details", array_values($form_state['values']['names_fieldset']));
    cache_clear_all();
  }
}

/**
 * This function will validate a string to check proper hexadecimal color.
 *
 * @param string $hex_color
 *   A string containing the RGB color for validation.
 */
function _keyword_highlighter_validate_hexacolor($hex_color) {
  if (preg_match('/#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/i', preg_quote($hex_color))) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}
