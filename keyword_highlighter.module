<?php
/**
 * @file
 * Keyword highlighter module by Dharmendra Patri
 */

/**
 * Implements hook_permission().
 */
function keyword_highlighter_permission() {
  return array(
    'administer kh' => array(
      'title' => t('Administer Keyword Highlighter'),
      'description' => t('Allow adminstrators to configure the list of keywords and color.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function keyword_highlighter_menu() {
  $items = array();
  $items['admin/config/content/keywords'] = array(
    'title' => 'Keyword Highlighter',
    'description' => 'Replaces words or phrases inside posts with filtered versions.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('keyword_highlighter_add_keywords_form'),
    'access callback' => 'user_access',
    'access arguments' => array('administer kh'),
    'file' => 'keyword_highlighter.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_filter_info().
 */
function keyword_highlighter_filter_info() {
  $filters['keyword_highlighter'] = array(
    'title' => t('Keyword highlighter filter'),
    'description' => t('Replace the keywords with provided color.'),
    'process callback' => '_keyword_highlighter_process',
    'weight' => 20,
  );
  return $filters;
}

/**
 * Process the text and apply the appropiate colors for the given keywords.
 */
function _keyword_highlighter_process($text, $filter, $format) {
  // Get data from data base and build the number of fields.
  $kh_data_array = variable_get('kh_data_details', '');
  $result = array();
  if (!empty($kh_data_array)) {
    $result = $kh_data_array;
  }

  return keyword_highlighter_replace_keywords($text, $result);
}

/**
 * This function will find and highlight the keyword with proper color.
 *
 * @param string $content
 *   A string containing the whole contenet.
 *
 * @param array $keyword_array
 *   An array of keywords and its values.
 */
function keyword_highlighter_replace_keywords($content, $keyword_array) {
  for ($i = 0; $i < count($keyword_array); $i++) {
    preg_match_all('/' . preg_quote($keyword_array[$i]['keyword']) . '/i', $content, $kh_matches);
    $kh_matches = array_unique($kh_matches);

    if (count($kh_matches) > 0) {
      // To avoid casesennsitive replacements, find each match and replace them.
      foreach ($kh_matches as $match) {
        $match = array_unique($match);
        foreach ($match as $amatch) {
          $replace_html = keyword_highlighter_generate_html($keyword_array[$i], $amatch);
          $content = preg_replace('/' . preg_quote($amatch) . '/', $replace_html, $content);
        }
      }
    }
  }
  return $content;
}

/**
 * Add necessary HTML to the keyword to show it as highlighted.
 * @param1 - Array of keyword (not Arrays) single element .
 */
function keyword_highlighter_generate_html($keyword, $ori_keyword) {
  return '<span style="color:' . check_plain($keyword['color']) . ';">'
  . check_plain($ori_keyword) . '</span>';
}
