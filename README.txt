Keyword Highlighter
================================

This module will help you highlight any keywords in your site. 
For example you have content driven website where you want to 
display few words ex(the name of your company, location, or any other) 
in a different color that the normal site color. Then this module will 
help you achieve that.

You can add as many keywords as you want and 
can define a color-code for them ex- #FF00CC

Install this module and you can manage the 
keywords at 
Configuration->Content Authoring->Keyword Highlighter

IMPORTANT INSTRUCTIONS TO FOLLOW
================================

You need to do the following setting in order for this module to work.
Follow the steps below after installing the Keyword Highlighter module.

1-> Go to Configuration->Content Authoring->Text Formats 
    Direct Link - admin/config/content/formats

2-> We need to configure every available text format there
    by default you will have Filtered HTML, Full HTML, Plain text

3-> Click on "configure" link for each format

4-> Under "Enabled filters" section, enable(check) 
    Keyword highlighter filter

5-> Below in this same configuration page, go to Filter processing order
    Make sure that "Keyword highlighter filter" 
		is always below "Limit allowed HTML tags"
		To make it easy, you may set "Keyword highlighter filter" 
		in the last position.

This is it, you can now add keywords and relavent colors. 
Feel free to report issues if any.		

Author
================================

Dharmendra Patri
http://drupal.org/user/204614
